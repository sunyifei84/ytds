package iamdev.me.ytds.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author zxc
 * @since 2018-06-22
 */
@TableName("tb_doc_recycle")
public class DocRecycle implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 回收id
     */
    @TableId(value = "recycle_id", type = IdType.AUTO)
    private Integer recycleId;
    /**
     * 文档id
     */
    @TableField("doc_id")
    private Integer docId;
    /**
     * 回收日期
     */
    @TableField("recycle_date")
    private Date recycleDate;


    public Integer getRecycleId() {
        return recycleId;
    }

    public void setRecycleId(Integer recycleId) {
        this.recycleId = recycleId;
    }

    public Integer getDocId() {
        return docId;
    }

    public void setDocId(Integer docId) {
        this.docId = docId;
    }

    public Date getRecycleDate() {
        return recycleDate;
    }

    public void setRecycleDate(Date recycleDate) {
        this.recycleDate = recycleDate;
    }

    @Override
    public String toString() {
        return "DocRecycle{" +
            ", recycleId=" + recycleId +
            ", docId=" + docId +
            ", recycleDate=" + recycleDate +
            "}";
    }
}
