package iamdev.me.ytds.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import iamdev.me.ytds.entity.ShareDoc;
import iamdev.me.ytds.mapper.ShareDocMapper;
import iamdev.me.ytds.service.IShareDocService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author zxc
 * @since 2018-06-22
 */
@Service
public class ShareDocServiceImpl extends ServiceImpl<ShareDocMapper, ShareDoc> implements
    IShareDocService {

}
