package iamdev.me.ytds.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import iamdev.me.ytds.entity.LoginLog;
import iamdev.me.ytds.mapper.LoginLogMapper;
import iamdev.me.ytds.service.ILoginLogService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author zxc
 * @since 2018-06-22
 */
@Service
public class LoginLogServiceImpl extends ServiceImpl<LoginLogMapper, LoginLog> implements
    ILoginLogService {

}
